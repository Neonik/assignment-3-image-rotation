#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include <./image.h>

struct image_t rotate90(struct image_t* img);

#endif //IMAGE_TRANSFORMER_ROTATE_H
