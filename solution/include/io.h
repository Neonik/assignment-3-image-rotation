#ifndef IMAGE_TRANSFORMER_IO_H
#define IMAGE_TRANSFORMER_IO_H

#include <./image.h>
#include <stdbool.h>
#include  <stdint.h>
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_BITS,
    READ_INVALID_SIGNATURE,
    READ_INVALID_MEMORY_ALLOCATION,
    READ_INVALID_BIT_COUNT,
    FAILED_TO_ALLOCATE_MEMORY_FOR_IMAGE
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR_HEADER,
    WRITE_ERROR_BITS
};

enum read_status from_bmp(FILE *in, struct image_t *img);

bool print_read_status(enum read_status status);

enum write_status to_bmp(FILE *out, struct image_t const *img);

bool print_write_status(enum write_status status);

#endif //IMAGE_TRANSFORMER_IO_H
