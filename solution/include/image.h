#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdbool.h>
#include <stdint.h>

struct pixel_t {
    uint8_t r, g, b;
};

struct image_t {
    uint64_t width, height;
    struct pixel_t *data;
};

struct image_t create_image(uint64_t width, uint64_t height);

void delete_image(struct image_t *img);

#endif //IMAGE_TRANSFORMER_IMAGE_H
