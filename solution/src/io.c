#include <bmp_image.h>
#include <io.h>
#include <stdbool.h>

#define HEADER_TYPE 0x4d42
#define HEADER_PLANES 1
#define HEADER_RESERVED 0
#define HEADER_SIZE  40
#define HEADER_BIT_COUNT 24
#define HEADER_PIXELS_PER_METER 2835
#define HEADER_CLR_IMPORTANT 0
#define HEADER_CLR_USED 0
#define HEADER_COMPRESSION 0


uint8_t get_padding(const uint64_t width) {
    return (-1 * (width * sizeof(struct pixel_t))) % 4;
}

enum read_status from_bmp(FILE *in, struct image_t *img) {
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (header.biBitCount != HEADER_BIT_COUNT) {
        return READ_INVALID_BIT_COUNT;
    }
    if (header.bfType != HEADER_TYPE) {
        return READ_INVALID_SIGNATURE;
    }

    *img = create_image(header.biWidth, header.biHeight);
    uint8_t padding_size = get_padding(img->width);

    if (!img->data) {
        return READ_INVALID_MEMORY_ALLOCATION;
    }

    for (uint32_t i = 0; i < img->height; i++) {
        if (!fread(&img->data[i * img->width], sizeof(struct pixel_t), img->width, in)) {
            delete_image(img);
            return READ_INVALID_BITS;
        }
        fseek(in, padding_size, SEEK_CUR);
    }

    return READ_OK;
}

bool print_read_status(const enum read_status status) {
    switch (status) {
        case READ_OK:
            printf("Reading was successful\n");
            return true;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "File has an invalid signature\n");
            return false;
        case READ_INVALID_HEADER:
            fprintf(stderr, "File has incorrect headers\n");
            return false;
        case READ_INVALID_BITS:
            fprintf(stderr, "Failed to read pixel data\n");
            return false;
        case READ_INVALID_MEMORY_ALLOCATION:
            fprintf(stderr, "Unable to allocate memory\n");
            return false;
        case FAILED_TO_ALLOCATE_MEMORY_FOR_IMAGE:
            fprintf(stderr, "Failed to allocate memory for the image\n");
            return false;
        default:
            return false;
    }
}

enum write_status to_bmp(FILE *out, const struct image_t *img) {
    uint64_t h = img->height;
    uint64_t w = img->width;

    uint8_t padding_size = get_padding(w);
    uint32_t row_with_padding = w * sizeof(struct pixel_t) + padding_size;

    struct bmp_header header = {
            .bfType = HEADER_TYPE,
            .biSizeImage = row_with_padding * h,
            .biWidth = w,
            .biHeight = h,
            .bOffBits = sizeof(struct bmp_header),
            .bfileSize = sizeof(struct bmp_header) + row_with_padding * h,
            .bfReserved = HEADER_RESERVED,
            .biSize = HEADER_SIZE,
            .biPlanes = HEADER_PLANES,
            .biBitCount = HEADER_BIT_COUNT,
            .biCompression = HEADER_COMPRESSION,
            .biYPelsPerMeter = HEADER_PIXELS_PER_METER,
            .biXPelsPerMeter = HEADER_PIXELS_PER_METER,
            .biClrImportant = HEADER_CLR_IMPORTANT,
            .biClrUsed = HEADER_CLR_USED
    };

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR_HEADER;
    }

    for (uint32_t i = 0; i < h; ++i) {
        if (fwrite(&img->data[w * i], sizeof(struct pixel_t), w, out) != w) {
            return WRITE_ERROR_BITS;
        }

        fseek(out, padding_size, SEEK_CUR);
    }

    return WRITE_OK;
}

bool print_write_status(const enum write_status st) {
    switch (st) {
        case WRITE_OK:
            printf("Data recording was successful\n");
            return true;
        case WRITE_ERROR_BITS:
            fprintf(stderr, "Failed to record pixels\n");
            return false;
        case WRITE_ERROR_HEADER:
            fprintf(stderr, "Failed to record headers\n");
            return false;
        default:
            return false;
    }
}
