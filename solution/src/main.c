#include <stdio.h>
#include <stdlib.h>

#include "image.h"
#include "io.h"
#include "rotate.h"

int main(int argc, char** argv) {
    if (argc < 4) {
        fprintf(stderr, "Not enough arguments\n");
        return -1;
    }

    FILE* in = fopen(argv[1], "rb");
    if (in == NULL) {
        fprintf(stderr, "Unable to open file\n");
        return -1;
    }

    struct image_t img;
    enum read_status status_r = from_bmp(in, &img);
    fclose(in);

    if (!print_read_status(status_r)) {
        return -1;
    }

    char *p;
    long converted = strtol(argv[3], &p, 10);
    if (*p != '\0') {
        printf("Cannot parse angle\n");
        return -1;
    }

    int32_t angle = (360 - ((int32_t) converted)) % 360;
    if (angle % 90 != 0) {
        printf("Wrong angle was passed to the input\n");
        return -1;
    }

    struct image_t new_img = img;
    for (uint32_t i = 0; i < angle / 90; i++) {
        new_img = rotate90(&new_img);
    }

    FILE* out = fopen(argv[2], "wb");
    if (out == NULL) {
        printf("Failed to open target file\n");
        return -1;
    }

    enum write_status status_w = to_bmp(out, &new_img);
    fclose(out);
    delete_image(&new_img);

    if (!print_write_status(status_w)) {
        return -1;
    }

    return 0;
}
