#include "rotate.h"
#include "image.h"

struct image_t rotate90(struct image_t* img) {
    struct image_t new_img = create_image(img->height, img->width);

    for (uint64_t i = 0; i < img->height; i++) {
        for(uint64_t j = 0; j < img->width; j++) {
            new_img.data[(j + 1) * new_img.width - i - 1] = img->data[j + i * img->width];
        }
    }

    delete_image(img);
    return new_img;
}
