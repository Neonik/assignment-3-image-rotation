#include <image.h>
#include <stdlib.h>

struct image_t create_image(const uint64_t width, const uint64_t height) {
    struct pixel_t *data = malloc(sizeof(struct pixel_t) * width * height);
    if (!data) return (struct image_t) {0};
    return (struct image_t) {
        .height = height,
        .width = width,
        .data =data
    };
}

void delete_image(struct image_t *img) {
    if (img->data) {
        free(img->data);
        img->data = NULL;
    }
}
